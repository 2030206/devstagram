@extends('layouts.app')

@section('titulo')
    Listado de alumnos
@endsection

@section('contenido')
<div class="container mx-auto px-4">
    <div class="py-8">
        <div class="mb-6">
            <h2 class="text-2xl font-bold">Listado de alumnos</h2>
        </div>
        <!-- Formulario de registro -->
        <form action="{{ route('alumnos') }}" method="POST" novalidate class="mt-8">
            @csrf
            <div class="grid grid-cols-1 sm:grid-cols-2 gap-6">
                <div>
                    <label for="nombre" class="block text-sm font-medium text-gray-700">Nombre</label>
                    <input id="nombre" name="nombre" type="text" placeholder="Tu nombre" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md @error('nombre') border-red-500 @enderror" value="{{ old('nombre') }}" />
                    @error('nombre')
                    <p class="text-red-500 mt-1 text-sm">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <label for="apellido" class="block text-sm font-medium text-gray-700">Apellido</label>
                    <input id="apellido" name="apellido" type="text" placeholder="Tu apellido" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md @error('apellido') border-red-500 @enderror" value="{{ old('apellido') }}" />
                    @error('apellido')
                    <p class="text-red-500 mt-1 text-sm">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <label for="fecha_nacimiento" class="block text-sm font-medium text-gray-700">Fecha de nacimiento</label>
                    <input id="fecha_nacimiento" name="fecha_nacimiento" type="date" placeholder="Tu fecha de nacimiento" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md @error('fecha_nacimiento') border-red-500 @enderror" value="{{ old('fecha_nacimiento') }}" />
                    @error('fecha_nacimiento')
                    <p class="text-red-500 mt-1 text-sm">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <label for="grupo_id" class="block text-sm font-medium text-gray-700">Grupo</label>
                    <select id="grupo_id" name="grupo_id" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md @error('grupo_id') border-red-500 @enderror">
                        <option value="">Seleccione el grupo</option>
                        @foreach ($grupos as $grupo)
                        <option value="{{ $grupo->id }}">{{ $grupo->grupo }}</option>
                        @endforeach
                    </select>
                    @error('grupo_id')
                    <p class="text-red-500 mt-1 text-sm">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <button type="submit" class="mt-8 bg-indigo-500 hover:bg-indigo-600 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 text-white font-bold py-2 px-4 rounded">Registrar alumno</button>
        </form>


         <!-- Tabla para ver las publicaciones -->
         <div class="overflow-hidden rounded-lg shadow-xs">
            <div class="overflow-x-auto">
                <table class="w-full table-auto">
                    <thead>
                        <tr class="border-b">
                            <th class="px-4 py-2">ID</th>
                            <th class="px-4 py-2">Nombre</th>
                            <th class="px-4 py-2">Apellidos</th>
                            <th class="px-4 py-2">Fecha de Nacimiento</th>
                            <th class="px-4 py-2">Grupo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($alumnos as $alumno)
                        <tr class="text-gray-700">
                            <td class="px-4 py-2">{{ $alumno->id }}</td>
                            <td class="px-4 py-2">{{ $alumno->nombre }}</td>
                            <td class="px-4 py-2">{{ $alumno->apellido }}</td>
                            <td class="px-4 py-2">{{ $alumno->fecha_nacimiento }}</td>
                            <!-- mostrar grupo en lugar de id  -->
                            <td class="px-4 py-2">{{ $alumno->grupos->grupo }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection