@extends('layouts.app')

@section('titulo')
    Listado de grupos
@endsection

@section('contenido')
<div class="container mx-auto px-4">
    <div class="py-8">
        <div class="mb-6">
            <h2 class="text-2xl font-bold">Listado de grupos</h2>
        </div>

        <!-- Formulario de registro -->
        <form action="{{ route('grupos') }}" method="POST" novalidate class="mt-8">
            @csrf
            <div class="mb-5">
                <label for="grupo" class="block text-sm font-medium text-gray-700">Nombre del grupo</label>
                <input id="grupo" name="grupo" type="text" placeholder="Nombre del grupo" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md @error('grupo') border-red-500 @enderror" value="{{ old('grupo') }}" />
                @error('grupo')
                <p class="text-red-500 mt-1 text-sm">{{ $message }}</p>
                @enderror
            </div>

            <button type="submit" class="mt-8 bg-indigo-500 hover:bg-indigo-600 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 text-white font-bold py-2 px-4 rounded">Registrar grupo</button>
        </form>

        
        <!-- Tabla para ver los grupos -->
        <div class="overflow-hidden rounded-lg shadow-xs">
            <div class="overflow-x-auto">
                <table class="w-full table-auto">
                    <thead>
                        <tr class="border-b">
                            <th class="px-4 py-2">ID</th>
                            <th class="px-4 py-2">Nombre del grupo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($grupos as $grupo)
                        <tr class="text-gray-700">
                            <td class="px-4 py-2">{{ $grupo->id }}</td>
                            <td class="px-4 py-2">{{ $grupo->grupo }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
@endsection