<?php

namespace App\Models;

use App\Models\Grupo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class alumno extends Model
{
    use HasFactory;

    //Se protege la info para guardarla en la base de datos 
    protected $fillable=[
        'nombre',
        'apellido',
        'fecha_nacimiento',
        'grupo_id'
    ];

    //se crean las relaciones 
    public function grupos()
    {

        //tal alumno pertenece a cierto grupo
        return $this->belongsTo(Grupo::class, 'grupo_id');
    }

    
}