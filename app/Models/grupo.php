<?php

namespace App\Models;

use App\Models\Alumno;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class grupo extends Model
{
    use HasFactory;

    //Se protege la info para guardarla en la base de datos 
    protected $fillable=[
        'grupo'
    ];

    public function grupos()
    {
        //Un grupo puede tener muchos alumnos
        return $this->hasMany(Alumno::class);
    }
}